package com.example.mongoDbStarter;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

@SpringBootApplication
public class MongoDbStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(MongoDbStarterApplication.class, args);
	}

	@Bean
	CommandLineRunner runner(StudentRepository repository){
		return args -> {
			Address address = new Address(
					"USA",
					"2324",
					"Port Charlotte"
			);

			Student student = new Student(
					"Sam",
					"Sanders",
					"thisemail@gmail.com",
					Gender.MALE,
					address,
					List.of("Computer things"),
					BigDecimal.TEN,
					LocalDateTime.now()

			);

			repository.insert(student);
		};
	}
}
